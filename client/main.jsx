import React from 'react';
import { render } from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Game } from '/imports/ui/Game';

Meteor.startup(() => {
  render(<Game/>, document.getElementById('react-target'));
});
