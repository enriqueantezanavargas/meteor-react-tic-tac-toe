import React, { useState } from 'react';
import styled from 'styled-components';
import { Board } from './Board';

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

export const Game = (props) => {
  const [ gameState, setGameState ] = useState({
    history: [{
      squares: Array(9).fill(null),
    }],
    stepNumber: 0,
    xIsNext: true,
  });
  const history = gameState.history;
  const current = history[gameState.stepNumber];
  const winner = calculateWinner(current.squares);
  let status;
  if (winner) {
    status = `Winner: ${winner}`;
  } else {
    status = `Next player: ${gameState.xIsNext ? 'X' : 'O'}`;
  }

  const jumpTo = (step) => {
    setGameState({
      ...gameState,
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    });
  };

  const handleClick = (i) => {
    const history = gameState.history.slice(0, gameState.stepNumber + 1);
    const current = history[gameState.stepNumber];
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = gameState.xIsNext ? 'X' : 'O';
    setGameState({
      history: history.concat([{
        squares: squares,
      }]),
      stepNumber: history.length,
      xIsNext: !gameState.xIsNext,
    });
  }

  const moves = history.map((step, move) => {
    const desc = move ? `Go to move #${move}` : 'Go to game start';
    return (
      <li key={move} style={{ paddingLeft: 30 }}>
        <button onClick={() => jumpTo(move)}>{desc}</button>
      </li>
    );
  });

  return (
    <GameContainer>
      <div className="game-board">
        <Board
          squares={current.squares}
          handleClick={(i) => handleClick(i)}
        />
      </div>
      <GameInfo>
        <div>{status} - {gameState.stepNumber}</div>
        <ol style={{ paddingLeft: 30 }}>{moves}</ol>
      </GameInfo>
    </GameContainer>
  );
}

const GameContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const GameInfo = styled.div`
  margin-left: 20px;
`;
