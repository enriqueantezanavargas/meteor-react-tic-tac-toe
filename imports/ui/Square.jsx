import React from 'react';
import styled from 'styled-components';

export const Square = (props) => {
  return (
    <SquareButton className="square" onClick={props.onClick}>
      {props.value}
    </SquareButton>
  );
}

const SquareButton = styled.button`
  background: #fff;
  border: 1px solid #999;
  float: left;
  font-size: 24px;
  font-weight: bold;
  line-height: 34px;
  height: 34px;
  margin-right: -1px;
  margin-top: -1px;
  padding: 0;
  text-align: center;
  width: 34px;
`;
