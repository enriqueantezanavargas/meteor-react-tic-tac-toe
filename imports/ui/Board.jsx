import React, { useState } from 'react';
import styled from 'styled-components';
import { Square } from './Square';

export const Board = (props) => {
  const renderSquare = (i) => {
    return (
      <Square
        value={props.squares[i]}
        onClick={() => props.handleClick(i)}
      />
    );
  }

  return (
    <div>
      <BoardRow>
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </BoardRow>
      <BoardRow>
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </BoardRow>
      <BoardRow>
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </BoardRow>
    </div>
  );
}

const BoardRow = styled.div`
  :after {
    clear: both;
    content: "";
    display: table;
  }

  :focus {
    outline: none;
  }
`;